package com.victormanuelclonkei.tiendacastelar.UI;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.victormanuelclonkei.tiendacastelar.R;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.ClienteController;


/**
 * A simple {@link Fragment} subclass.
 */
public class InsertClientesFragment extends Fragment {


    private static final String TAG = "ClientesWeb";

    EditText etIdCliente;
    EditText etNombre;
    EditText etApellido1;
    EditText etApellido2;
    EditText etNif;
    CheckBox cbVaron;
    int varon;
    EditText etNumCta;
    EditText etComoNosConocio;
    CheckBox cbActivo;
    int activo;


    public InsertClientesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_insert_clientes, container, false);
        //Inicializamos los view
        etIdCliente = (EditText) view.findViewById(R.id.etIdCliente);
        etIdCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etIdCliente.setText("");
            }
        });
        etNombre = (EditText) view.findViewById(R.id.etNombre);
        etNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNombre.setText("");
            }
        });
        etApellido1 = (EditText) view.findViewById(R.id.etApellido1);
        etApellido1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etApellido1.setText("");
            }
        });
        etApellido2 = (EditText) view.findViewById(R.id.etApellido2);
        etApellido2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etApellido2.setText("");
            }
        });
        etNif = (EditText) view.findViewById(R.id.etNif);
        etNif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNif.setText("");
            }
        });
        cbVaron = (CheckBox) view.findViewById(R.id.chckBoxVaron);
        cbVaron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbVaron.isChecked() == true) {
                    varon = 1;
                } else {
                    varon = 0;
                }
            }
        });
        etNumCta = (EditText) view.findViewById(R.id.etNumCta);
        etNumCta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etNumCta.setText("");
            }
        });
        etComoNosConocio = (EditText) view.findViewById(R.id.etComoNosConocio);
        etComoNosConocio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etComoNosConocio.setText("");
            }
        });
        cbActivo = (CheckBox) view.findViewById(R.id.chckBoxActivo);
        cbActivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbActivo.isChecked() == true) {
                    activo = 1;
                } else {
                    activo = 0;
                }
            }
        });


        Button btAdd = (Button) view.findViewById(R.id.btAdd);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertarCliente(view);
            }
        });
        Button btCancel = (Button) view.findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Operation Canceled", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                loadFramentMain();
            }
        });
        return view;
    }

    private void insertarCliente(View view) {
        ClienteController cc = new ClienteController(getActivity(), view); //Inicializamos controlador con contexto
        cc.insertCliente(etIdCliente.getText().toString(), etNombre.getText().toString(),
                etApellido1.getText().toString(), etApellido2.getText().toString(),
                etNif.getText().toString(), varon, etNumCta.getText().toString(),
                etComoNosConocio.getText().toString(), activo);
        loadFramentMain();
    }

    private void loadFramentMain() {
        Fragment mainActivityFragment = new MainActivityFragment();

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mainActivityFragment)
                .addToBackStack(mainActivityFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
}


