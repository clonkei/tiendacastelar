package com.victormanuelclonkei.tiendacastelar.Entities;

import java.io.Serializable;

public class Cliente implements Serializable {
    private int id;
    private int id_cliente;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String nif;
    private boolean varon;
    private String numCta;
    private String comoNosConocio;
    private boolean activo;

    public Cliente(int id, int idCliente, String nombre, String apellido1, String apellido2, String nif, boolean varon, String numCta, String comoNosConocio, boolean activo) {
        this.id = id;
        this.id_cliente = idCliente;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.nif = nif;
        this.varon = varon;
        this.numCta = numCta;
        this.comoNosConocio = comoNosConocio;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return id_cliente;
    }

    public void setIdCliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public boolean isVaron() {
        return varon;
    }

    public void setVaron(boolean varon) {
        this.varon = varon;
    }

    public String getNumCta() {
        return numCta;
    }

    public void setNumCta(String numCta) {
        this.numCta = numCta;
    }

    public String getComoNosConocio() {
        return comoNosConocio;
    }

    public void setComoNosConocio(String comoNosConocio) {
        this.comoNosConocio = comoNosConocio;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", idCliente=" + id_cliente +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", nif='" + nif + '\'' +
                ", varon=" + varon +
                ", numCta='" + numCta + '\'' +
                ", comoNosConocio='" + comoNosConocio + '\'' +
                ", activo=" + activo +
                '}';
    }
}
