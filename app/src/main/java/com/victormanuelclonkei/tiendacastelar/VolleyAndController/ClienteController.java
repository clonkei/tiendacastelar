package com.victormanuelclonkei.tiendacastelar.VolleyAndController;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victormanuelclonkei.tiendacastelar.DB.Constant;
import com.victormanuelclonkei.tiendacastelar.Entities.Cliente;
import com.victormanuelclonkei.tiendacastelar.UI.ClienteAdapter;
import com.victormanuelclonkei.tiendacastelar.UI.RecyclerViewClientesAdapter;
import com.victormanuelclonkei.tiendacastelar.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ClienteController {
    private static final String TAG = "ClientesWeb";
    private Context context; //Contexto desde donde se está realizando la acción
    private View view; //View donde se está ejecutando
    private Gson gson = new Gson();

    public ClienteController(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public void insertCliente(String idCliente, String nombre, String apellido1, String apellido2, String nif, int varon, String numCta, String comoNosConocio, int activo) {
        final boolean resultado = false;

        HashMap<String, String> map = new HashMap<>();
        map.put("id_cliente", idCliente);
        map.put("nombre", nombre);
        map.put("apellido1", apellido1);
        map.put("apellido2", apellido2);
        map.put("nif", nif);
        map.put("varon", Integer.toString(varon));
        map.put("numcta", numCta);
        map.put("como_nos_conocio", comoNosConocio);
        map.put("activo", Integer.toString(activo));
        //Calculamos fecha
        /*Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String timestamp = simpleDateFormat.format(now);
        map.put("fecha", timestamp);*/
        JSONObject jsonObject = new JSONObject(map); // Crear nuevo objeto Json basado en el mapa
        Log.d("AAAAAAAAAAAAAAAA", jsonObject.toString());
        //Volley
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constant.urlClienteCreate, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        procesarInsercion(response); // Procesar la respuesta Json
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Volley: " + error.getMessage());

                    }
                }); //Fin de JsonObjectRequest
        VolleySingleton singleton = VolleySingleton.getInstance(context);
        singleton.addToRequestQueue(request);
    } //Fin insertDireccionesCliente

    private void procesarInsercion(JSONObject response) {
        try {
            switch (response.getString("message")) {
                case "201": // Correcto
                    Snackbar.make(view, "Inserción correcta", Snackbar.LENGTH_LONG).show();
                    break;
                case "400": // error
                    Snackbar.make(view, "No se ha creado el elemento. Falta información", Snackbar.LENGTH_LONG).show();
                    break;
                case "503": // error
                    Snackbar.make(view, "No se ha creado el elemento. Servicio no disponible. ", Snackbar.LENGTH_LONG).show();
                    break;
                default:
                    Snackbar.make(view, "No se ha creado el elemento. Error general. ", Snackbar.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //Leer todos los mensajes mediante Volley. Obtener un resultado y procesarlo
    public void simpleGetClientes() {
        VolleySingleton
                .getInstance(context)
                .addToRequestQueue(
                        new JsonObjectRequest(Request.Method.GET, Constant.urlClienteReadAll, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResult(response); // Procesar la respuesta Json
                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error.networkResponse.statusCode == 404) {
                                            Toast.makeText(context, "No hay registros en la base de datos", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Log.d(TAG, "Error en la ejecución de Volley: " + error.getMessage());
                                        }
                                    }
                                }
                        ));
    }

    private void parseResult(JSONObject response) {
        try {
            if (response.getString("body") != null) {
                JSONArray body = response.getJSONArray("body");
                Type listType = new TypeToken<ArrayList<Cliente>>() {
                }.getType(); //Deserialización genérica
                List<Cliente> clientes = gson.fromJson(body.toString(), listType); // preparar con Gson
                Log.d("TEST", clientes.get(0).getNombre());
            } else {
                if (response.getString("message") != null) { //Si el webservice tiene algo que decirnos lo hace mediante un message con un código
                    String jsonMessage = response.getString("message");
                    Toast.makeText(context, jsonMessage, Toast.LENGTH_LONG).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace(); //Algún otro error más raro
        }

    }

    //Leer todos los mensajes mediante Volley. Obtener un resultado y procesarlo
    public void getClientes() {
        //1. Creamos un objeto JsonObjectRequest ( o JsonArrayRequest) para solicitar los datos al servicio y comenzar su proceso)
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Constant.urlClienteReadAll, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarConsulta(response); // Procesar la respuesta Json
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse.statusCode == 404) {
                            Toast.makeText(context, "No hay registros en la base de datos", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d(TAG, "Error en la ejecución de Volley: " + error.getMessage());
                        }
                    }
                }
        );
        //2. Ejecutamos la consulta. En este caso mediante el uso de volley
        VolleySingleton singleton = VolleySingleton.getInstance(context);
        singleton.addToRequestQueue(request);
    }

    private void procesarConsulta(JSONObject response) {
        try {
            Log.d(TAG, "Respuesta: " + response.toString());
            //Si hay algo en el body es que hemos obtenido una respuesta
            if (response.getString("body") != null) {
                JSONArray body = response.getJSONArray("body");
                Cliente[] clientes = gson.fromJson(body.toString(), Cliente[].class); // preparar con Gson
                // Inicializar adaptador
                ClienteAdapter clienteAdapter = new ClienteAdapter(view.getContext(), (ArrayList<Cliente>) new ArrayList<>(Arrays.asList(clientes)));
                RecyclerView recyclerViewClientes = view.findViewById(R.id.rv_clientes);
                recyclerViewClientes.setAdapter(new RecyclerViewClientesAdapter(new ArrayList<>(Arrays.asList(clientes)),context));
                recyclerViewClientes.setLayoutManager(new LinearLayoutManager(context));
            } else {
                //Si el webservice tiene algo que decirnos lo hace mediante un message con un código
                if (response.getString("message") != null) {
                    String jsonMessage = response.getString("message");
                    Toast.makeText(
                            context,
                            jsonMessage,
                            Toast.LENGTH_LONG).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace(); //Algún otro error más raro
        }
    }

    public void deleteCliente(int id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(id));
        JSONObject jsonObject = new JSONObject(map); // Crear nuevo objeto Json basado en el mapa

        //Volley
        VolleySingleton.getInstance(context).addToRequestQueue(
                new JsonObjectRequest(Request.Method.POST, Constant.urlClienteDelete, jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //procesarBorrado(response); // Procesar la respuesta Json
                                Log.d(TAG, "Borrado correcto");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        })  //Fin de JsonObjectRequest
        );
    } //Fin insert_mensaje

}