package com.victormanuelclonkei.tiendacastelar.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.victormanuelclonkei.tiendacastelar.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModificarDireccionesClientesFragment extends Fragment {


    public ModificarDireccionesClientesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modificar_direcciones_clientes, container, false);
    }

}
