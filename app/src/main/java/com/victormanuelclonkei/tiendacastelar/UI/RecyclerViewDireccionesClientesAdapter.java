package com.victormanuelclonkei.tiendacastelar.UI;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.victormanuelclonkei.tiendacastelar.Entities.DireccionCliente;
import com.victormanuelclonkei.tiendacastelar.R;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.ClienteController;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.DireccionesClienteController;

import java.util.ArrayList;

public class RecyclerViewDireccionesClientesAdapter extends RecyclerView.Adapter<RecyclerViewDireccionesClientesAdapter.ViewHolder>{
Context context;
    private ArrayList<DireccionCliente> direccionesClientes;
    private LayoutInflater miInflater;

    public RecyclerViewDireccionesClientesAdapter(ArrayList<DireccionCliente> direccionesClientes, Context context) {
        this.direccionesClientes = direccionesClientes;
        miInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerViewDireccionesClientesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = miInflater.inflate(R.layout.recyclerview_direcciones_clientes, viewGroup, false);
        return new RecyclerViewDireccionesClientesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewDireccionesClientesAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvIdCliente.setText(Integer.toString(direccionesClientes.get(i).getId_cliente()));
        viewHolder.tvLocalidad.setText(direccionesClientes.get(i).getLocalidad());
        viewHolder.tvCalle.setText(direccionesClientes.get(i).getCalle());
        viewHolder.tvPais.setText(direccionesClientes.get(i).getPais());
        viewHolder.btBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AAAAAAAAAAAAAAAAA", "intentando borrar direccion cliente");
                DireccionesClienteController dc = new DireccionesClienteController(context, v);
                dc.deleteDireccionCliente(direccionesClientes.get(i).getId());
                Log.d("AAAAAAAAAAAAAAAAA", String.valueOf(direccionesClientes.get(i).getId()));
                direccionesClientes.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, getItemCount());
            }
        });
    }

    @Override
    public int getItemCount() {
        return direccionesClientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvIdCliente;
        TextView tvLocalidad;
        TextView tvCalle;
        TextView tvPais;
        Button btBorrar;
        public ViewHolder(View itemView) {
            super(itemView);
            tvIdCliente = itemView.findViewById(R.id.tv_id_cliente);
            tvLocalidad = itemView.findViewById(R.id.tv_localidad);
            tvCalle = itemView.findViewById(R.id.tv_calle);
            tvPais = itemView.findViewById(R.id.tv_pais);
            btBorrar = itemView.findViewById(R.id.bt_borrar);

        }
    }
}
