package com.victormanuelclonkei.tiendacastelar.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.victormanuelclonkei.tiendacastelar.Entities.Cliente;
import com.victormanuelclonkei.tiendacastelar.Entities.DireccionCliente;
import com.victormanuelclonkei.tiendacastelar.R;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.ClienteController;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.DireccionesClienteController;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainActivityFragment extends Fragment {


    public MainActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_activity, container, false);
        populateListViewClientes(view);
        populateListViewDireccionesClientes(view);
        return view;
    }


    public void populateListViewClientes(View view) {
        ClienteController cc = new ClienteController(getActivity(),view);
        RecyclerView recyclerViewClientes = view.findViewById(R.id.rv_clientes);
        recyclerViewClientes.setAdapter(new RecyclerViewClientesAdapter(new ArrayList<Cliente>(),getContext()));
        recyclerViewClientes.setLayoutManager(new LinearLayoutManager(getContext()));
        cc.getClientes();
    }
    public void populateListViewDireccionesClientes(View view) {
        DireccionesClienteController dc = new DireccionesClienteController(getActivity(),view);
        RecyclerView recyclerViewDireccionesClientes = view.findViewById(R.id.rv_direcciones_clientes);
        recyclerViewDireccionesClientes.setAdapter(new RecyclerViewDireccionesClientesAdapter(new ArrayList<DireccionCliente>(),getContext()));
        recyclerViewDireccionesClientes.setLayoutManager(new LinearLayoutManager(getContext()));
        dc.getDireccionesClientes();
    }

}
