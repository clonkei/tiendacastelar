package com.victormanuelclonkei.tiendacastelar.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.victormanuelclonkei.tiendacastelar.Entities.Cliente;
import com.victormanuelclonkei.tiendacastelar.R;

import java.util.ArrayList;

public class ClienteAdapter extends BaseAdapter {
    private Context context; //context
    private ArrayList<Cliente> clientes;

    public ClienteAdapter(Context context, ArrayList<Cliente> clientes) {
        this.context = context;
        this.clientes = clientes;
    }

    @Override
    public int getCount() {
        return clientes.size();
    }

    @Override
    public Object getItem(int i) {
        return clientes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.recyclerview_clientes, viewGroup, false);
        }
        Cliente cliente = (Cliente) getItem(i); //Elemento que se mostrará como actual
        //Obtener el textview del elemento
        TextView tvIdCliente = (TextView) view.findViewById(R.id.tv_id_cliente);
        TextView tvNombre = (TextView) view.findViewById(R.id.tv_nombre);
        TextView tvApellido = (TextView) view.findViewById(R.id.tv_apellido);
        TextView tvNif = (TextView) view.findViewById(R.id.tv_nif);
        //Establecer el contenido
        tvIdCliente.setText(String.valueOf(cliente.getIdCliente()));
        tvNombre.setText(cliente.getNombre());
        tvApellido.setText(cliente.getApellido1());
        tvNif.setText(cliente.getNif());
        //Devuelve la fila
        return view;
    }
}
