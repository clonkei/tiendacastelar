package com.victormanuelclonkei.tiendacastelar.VolleyAndController;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victormanuelclonkei.tiendacastelar.DB.Constant;
import com.victormanuelclonkei.tiendacastelar.Entities.DireccionCliente;
import com.victormanuelclonkei.tiendacastelar.UI.DireccionesClientesAdapter;
import com.victormanuelclonkei.tiendacastelar.UI.RecyclerViewDireccionesClientesAdapter;
import com.victormanuelclonkei.tiendacastelar.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class DireccionesClienteController {
    private static final String TAG = "DireccionesClientesWeb";
    private Context context; //Contexto desde donde se está realizando la acción
    private View view; //View donde se está ejecutando
    private Gson gson = new Gson();

    public DireccionesClienteController(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public void insertDireccionesCliente(String idCliente, String calle, String codPostal, String localidad, String provincia, String pais, int activo) {
        final boolean resultado = false;
        HashMap<String, String> map = new HashMap<>();
        map.put("id_cliente", idCliente);
        map.put("calle", calle);
        map.put("codpostal", codPostal);
        map.put("localidad", localidad);
        map.put("provincia", provincia);
        map.put("pais", pais);
        map.put("activo", Integer.toString(activo));
        //Calculamos fecha
        /*Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String timestamp = simpleDateFormat.format(now);
        map.put("fecha", timestamp);*/
        JSONObject jsonObject = new JSONObject(map); // Crear nuevo objeto Json basado en el mapa
        Log.d("AAAAAAAAAAAAAAAAA", jsonObject.toString());
        //Volley
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Constant.urlDireccionesClientesCreate, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        procesarInsercion(response); // Procesar la respuesta Json
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Volley: " + error.getMessage());

                    }
                }); //Fin de JsonObjectRequest
        VolleySingleton singleton = VolleySingleton.getInstance(context);
        singleton.addToRequestQueue(request);
    } //Fin insertCliente

    private void procesarInsercion(JSONObject response) {
        try {
            switch (response.getString("message")) {
                case "201": // Correcto
                    Snackbar.make(view, "Inserción correcta", Snackbar.LENGTH_LONG).show();
                    break;
                case "400": // error
                    Snackbar.make(view, "No se ha creado el elemento. Falta información", Snackbar.LENGTH_LONG).show();
                    break;
                case "503": // error
                    Snackbar.make(view, "No se ha creado el elemento. Servicio no disponible. ", Snackbar.LENGTH_LONG).show();
                    break;
                default:
                    Snackbar.make(view, "No se ha creado el elemento. Error general. ", Snackbar.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //Leer todos los mensajes mediante Volley. Obtener un resultado y procesarlo
    public void simpleGetDireccionesCliente() {
        VolleySingleton
                .getInstance(context)
                .addToRequestQueue(
                        new JsonObjectRequest(Request.Method.GET, Constant.urlDireccionesClientesReadAll, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                parseResult(response); // Procesar la respuesta Json
                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error.networkResponse.statusCode == 404) {
                                            Toast.makeText(context, "No hay registros en la base de datos", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Log.d(TAG, "Error en la ejecución de Volley: " + error.getMessage());
                                        }
                                    }
                                }
                        ));
    }

    private void parseResult(JSONObject response) {
        try {
            if (response.getString("body") != null) {
                JSONArray body = response.getJSONArray("body");
                Type listType = new TypeToken<ArrayList<DireccionCliente>>() {
                }.getType(); //Deserialización genérica
                List<DireccionCliente> direccionClientes = gson.fromJson(body.toString(), listType); // preparar con Gson
                Log.d("TEST", direccionClientes.get(0).getCalle());
            } else {
                if (response.getString("message") != null) { //Si el webservice tiene algo que decirnos lo hace mediante un message con un código
                    String jsonMessage = response.getString("message");
                    Toast.makeText(context, jsonMessage, Toast.LENGTH_LONG).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace(); //Algún otro error más raro
        }

    }
    //Leer todos los mensajes mediante Volley. Obtener un resultado y procesarlo
    public void getDireccionesClientes() {
        //1. Creamos un objeto JsonObjectRequest ( o JsonArrayRequest) para solicitar los datos al servicio y comenzar su proceso)
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Constant.urlDireccionesClientesReadAll, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                procesarConsulta(response); // Procesar la respuesta Json
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse.statusCode == 404) {
                            Toast.makeText(context, "No hay registros en la base de datos", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d(TAG, "Error en la ejecución de Volley: " + error.getMessage());
                        }
                    }
                }
        );
        //2. Ejecutamos la consulta. En este caso mediante el uso de volley
        VolleySingleton singleton = VolleySingleton.getInstance(context);
        singleton.addToRequestQueue(request);
    }

    private void procesarConsulta(JSONObject response) {
        try {
            Log.d(TAG, "Respuesta: " + response.toString());
            //Si hay algo en el body es que hemos obtenido una respuesta
            if (response.getString("body") != null) {
                JSONArray body = response.getJSONArray("body");
                DireccionCliente[] direccionesClientes = gson.fromJson(body.toString(), DireccionCliente[].class); // preparar con Gson
                // Inicializar adaptador
                DireccionesClientesAdapter direccionesClientesAdapter = new DireccionesClientesAdapter(view.getContext(), (ArrayList<DireccionCliente>) new ArrayList<>(Arrays.asList(direccionesClientes)));
                RecyclerView recyclerViewDireccionesClientes = view.findViewById(R.id.rv_direcciones_clientes);
                recyclerViewDireccionesClientes.setAdapter(new RecyclerViewDireccionesClientesAdapter(new ArrayList<>(Arrays.asList(direccionesClientes)),context));
                recyclerViewDireccionesClientes.setLayoutManager(new LinearLayoutManager(context));
            } else {
                //Si el webservice tiene algo que decirnos lo hace mediante un message con un código
                if (response.getString("message") != null) {
                    String jsonMessage = response.getString("message");
                    Toast.makeText(
                            context,
                            jsonMessage,
                            Toast.LENGTH_LONG).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace(); //Algún otro error más raro
        }
    }

    public void deleteDireccionCliente(int id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(id));
        JSONObject jsonObject = new JSONObject(map);
        // Crear nuevo objeto Json basado en el mapa
        Log.d(TAG, jsonObject.toString());
        //Volley
        VolleySingleton.getInstance(context).addToRequestQueue(
                new JsonObjectRequest(Request.Method.POST, Constant.urlDireccionesClientesDelete, jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //procesarBorrado(response); // Procesar la respuesta Json
                                Log.d(TAG, "Borrado correcto");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        })  //Fin de JsonObjectRequest
        );
    } //Fin insert_mensaje
}
