package com.victormanuelclonkei.tiendacastelar.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.victormanuelclonkei.tiendacastelar.Entities.DireccionCliente;
import com.victormanuelclonkei.tiendacastelar.R;

import java.util.ArrayList;

public class DireccionesClientesAdapter extends BaseAdapter {
    private Context context; //context
    private ArrayList<DireccionCliente> direccionClientes;

    public DireccionesClientesAdapter(Context context, ArrayList<DireccionCliente> direccionClientes) {
        this.context = context;
        this.direccionClientes = direccionClientes;
    }

    @Override
    public int getCount() {
        return direccionClientes.size();
    }

    @Override
    public Object getItem(int i) {
        return direccionClientes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.recyclerview_direcciones_clientes, viewGroup, false);
        }
        DireccionCliente direccionCliente = (DireccionCliente) getItem(i); //Elemento que se mostrará como actual
        //Obtener el textview del elemento
        TextView tvIdCliente = (TextView) view.findViewById(R.id.tv_id_cliente);
        TextView tvLocalidad = (TextView) view.findViewById(R.id.tv_localidad);
        TextView tvCalle = (TextView) view.findViewById(R.id.tv_calle);
        TextView tvPais = (TextView) view.findViewById(R.id.tv_pais);
        //Establecer el contenido
        tvIdCliente.setText(String.valueOf(direccionCliente.getId_cliente()));
        tvLocalidad.setText(direccionCliente.getLocalidad());
        tvCalle.setText(direccionCliente.getCalle());
        tvPais.setText(direccionCliente.getPais());
        //Devuelve la fila
        return view;
    }
}
