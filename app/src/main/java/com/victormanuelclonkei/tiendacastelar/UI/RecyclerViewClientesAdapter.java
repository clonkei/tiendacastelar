package com.victormanuelclonkei.tiendacastelar.UI;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.victormanuelclonkei.tiendacastelar.Entities.Cliente;
import com.victormanuelclonkei.tiendacastelar.R;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.ClienteController;

import java.util.ArrayList;

public class RecyclerViewClientesAdapter extends RecyclerView.Adapter<RecyclerViewClientesAdapter.ViewHolder>{
private Context context;
    private ArrayList<Cliente> clientes;
    private LayoutInflater miInflater;

    public RecyclerViewClientesAdapter(ArrayList<Cliente> clientes, Context context) {
        this.clientes = clientes;
        miInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerViewClientesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = miInflater.inflate(R.layout.recyclerview_clientes, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewClientesAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tvIdCliente.setText(Integer.toString(clientes.get(i).getIdCliente()));
        viewHolder.tvNombre.setText(clientes.get(i).getNombre());
        viewHolder.tvApellido.setText(clientes.get(i).getApellido1());
        viewHolder.tvNif.setText(clientes.get(i).getNif());
        viewHolder.btBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AAAAAAAAAAAAAAAAA", "intentando borrar cliente");
                ClienteController cc = new ClienteController(context, v);
                cc.deleteCliente(clientes.get(i).getId());
                clientes.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, getItemCount());
            }
        });
    }

    @Override
    public int getItemCount() {
        return clientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvIdCliente;
        TextView tvNombre;
        TextView tvApellido;
        TextView tvNif;
        Button btBorrar;
        public ViewHolder(View itemView) {
            super(itemView);
            tvIdCliente = itemView.findViewById(R.id.tv_id_cliente);
            tvNombre = itemView.findViewById(R.id.tv_nombre);
            tvApellido = itemView.findViewById(R.id.tv_apellido);
            tvNif = itemView.findViewById(R.id.tv_nif);
            btBorrar = itemView.findViewById(R.id.bt_borrar);





        }

    }
}
