package com.victormanuelclonkei.tiendacastelar.DB;

public class Constant {
    public static final String SERVERIP = "http://192.168.61.225";
    //public static final String SERVERIP = "http://192.168.1.80";
    public static final String PATH = "/tiendacastelar/webservice";

    //Clientes
    public static final String urlClienteReadAll = SERVERIP + PATH + "/clientes/read.php";
    public static final String urlClienteReadOne = SERVERIP + PATH + "/clientes/read_one.php";
    public static final String urlClienteCreate = SERVERIP + PATH + "/clientes/create.php";
    public static final String urlClienteDelete = SERVERIP + PATH + "/clientes/delete.php";
    public static final String urlClienteUpdate = SERVERIP + PATH + "/clientes/update.php";

    //Direcciones_Clientes
    public static final String urlDireccionesClientesReadAll = SERVERIP + PATH + "/direcciones_clientes/read.php";
    public static final String urlDireccionesClientesReadOne = SERVERIP + PATH + "/direcciones_clientes/read_one.php";
    public static final String urlDireccionesClientesCreate = SERVERIP + PATH + "/direcciones_clientes/create.php";
    public static final String urlDireccionesClientesDelete = SERVERIP + PATH + "/direcciones_clientes/delete.php";
    public static final String urlDireccionesClientesUpdate = SERVERIP + PATH + "/direcciones_clientes/update.php";

}
