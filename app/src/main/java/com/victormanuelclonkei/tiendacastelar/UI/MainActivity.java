package com.victormanuelclonkei.tiendacastelar.UI;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.victormanuelclonkei.tiendacastelar.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Insertando cliente", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                loadFramentInsert();
            }
        });
        FloatingActionButton fab2 = findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Insertando direccion de cliente", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                loadFramentInsert2();
            }
        });
        loadFramentMain();
    }
    private void loadFramentMain() {
        Fragment mainActivityFragment = new MainActivityFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mainActivityFragment)
                .addToBackStack(mainActivityFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void loadFramentInsert() {
        Fragment insertActivityFragment = new InsertClientesFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, insertActivityFragment)
                .addToBackStack(insertActivityFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
    private void loadFramentInsert2() {
        Fragment insertActivityFragment = new InsertDireccionesClientesFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, insertActivityFragment)
                .addToBackStack(insertActivityFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
}
