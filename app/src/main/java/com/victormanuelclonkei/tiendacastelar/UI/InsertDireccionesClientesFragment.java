package com.victormanuelclonkei.tiendacastelar.UI;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.victormanuelclonkei.tiendacastelar.R;
import com.victormanuelclonkei.tiendacastelar.VolleyAndController.DireccionesClienteController;

/**
 * A simple {@link Fragment} subclass.
 */
public class InsertDireccionesClientesFragment extends Fragment {


    private static final String TAG = "DireccionesClientesWeb";

    EditText etIdCliente;
    EditText etCalle;
    EditText etCodPostal;
    EditText etLocalidad;
    EditText etProvincia;
    EditText etPais;
    CheckBox cbActivo;
    int activo;


    public InsertDireccionesClientesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_insert_direcciones_clientes, container, false);
        //Inicializamos los view
        etIdCliente = (EditText) view.findViewById(R.id.etIdCliente);
        etIdCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etIdCliente.setText("");
            }
        });
        etCalle = (EditText) view.findViewById(R.id.etCalle);
        etCalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCalle.setText("");
            }
        });
        etCodPostal = (EditText) view.findViewById(R.id.etCodPostal);
        etCodPostal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCodPostal.setText("");
            }
        });
        etLocalidad = (EditText) view.findViewById(R.id.etLocalidad);
        etLocalidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etLocalidad.setText("");
            }
        });
        etProvincia = (EditText) view.findViewById(R.id.etProvincia);
        etProvincia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCodPostal.setText("");
            }
        });
        etPais = (EditText) view.findViewById(R.id.etPais);
        etPais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPais.setText("");
            }
        });
        cbActivo = (CheckBox) view.findViewById(R.id.chckBoxActivo);
        cbActivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cbActivo.isChecked()==true) {
                    activo = 1;
                } else {
                    activo = 0;
                }
            }
        });

        Button btAdd = (Button) view.findViewById(R.id.btAdd);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertarDireccionCliente(view);
            }
        });
        Button btCancel = (Button) view.findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Operation Canceled", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                loadFramentMain();
            }
        });
        return view;
    }

    private void insertarDireccionCliente(View view) {
        DireccionesClienteController dc = new DireccionesClienteController(getActivity(),view); //Inicializamos controlador con contexto
        dc.insertDireccionesCliente(etIdCliente.getText().toString(),etCalle.getText().toString(),
                etCodPostal.getText().toString(), etLocalidad.getText().toString(),
                etProvincia.getText().toString(), etPais.getText().toString(), activo);
        loadFramentMain();
    }

    private void loadFramentMain() {
        Fragment mainActivityFragment = new MainActivityFragment();

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, mainActivityFragment)
                .addToBackStack(mainActivityFragment.toString())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

}
