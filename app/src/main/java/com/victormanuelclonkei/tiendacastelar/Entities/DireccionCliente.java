package com.victormanuelclonkei.tiendacastelar.Entities;

import java.io.Serializable;

public class DireccionCliente implements Serializable {
    private int id;
    private int id_cliente;
    private String calle;
    private String codPostal;
    private String localidad;
    private String provincia;
    private String pais;
    private boolean activo;

    public DireccionCliente(int id, int idCliente, String calle, String codPostal, String localidad, String provincia, String pais, boolean activo) {
        this.id = id;
        this.id_cliente = idCliente;
        this.calle = calle;
        this.codPostal = codPostal;
        this.localidad = localidad;
        this.provincia = provincia;
        this.pais = pais;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "DireccionCliente{" +
                "id=" + id +
                ", id_cliente=" + id_cliente +
                ", calle='" + calle + '\'' +
                ", codPostal='" + codPostal + '\'' +
                ", localidad='" + localidad + '\'' +
                ", provincia='" + provincia + '\'' +
                ", pais='" + pais + '\'' +
                ", activo=" + activo +
                '}';
    }
}
